package co.md.repository;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import co.md.AnalyticsMasterApplicationTests;
import co.md.repository.RedshiftRepository;

@Ignore
public class RedshiftRepositoryTest extends AnalyticsMasterApplicationTests {

	@Autowired
	private RedshiftRepository redshiftRepository;
	
	@Before
	public void init() {
		Assert.assertNotNull(redshiftRepository);
	}
	
	@Test
	public void testCopyS3ToRedshift() {
		String dbUrl = "jdbc:redshift://md-log.cvkxxjip0qbg.us-east-1.redshift.amazonaws.com:5439/engine";
		String username = "md";
		String password = "Mangodata123$";
		redshiftRepository.copyFromS3ToRedshift(username, password, dbUrl);
	}
}