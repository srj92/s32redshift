package co.md.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import co.md.AnalyticsMasterApplicationTests;
import co.md.service.RedshiftService;

@Ignore
public class RedshiftServiceTest extends AnalyticsMasterApplicationTests {
	
	@Autowired
	private RedshiftService redshiftService;
	
	@Before
	public void init() {
		Assert.assertNotNull(redshiftService);
	}
	
	@Test
	public void testRedshiftCreateTable() {
		redshiftService.createTableForNextDay();
	}

}