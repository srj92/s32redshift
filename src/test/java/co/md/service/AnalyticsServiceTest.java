package co.md.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import co.md.AnalyticsMasterApplicationTests;
import co.md.service.AbstractAnalyticsService;
import co.md.util.DateTimeUtils;

@Ignore
public class AnalyticsServiceTest extends AnalyticsMasterApplicationTests {

	@Autowired
	private AbstractAnalyticsService analyticsService;
	
	@Before
	public void init() {
		Assert.assertNotNull(analyticsService);
	}
	
	@Test
	public void testAnalyticsService() {
		analyticsService.process(DateTimeUtils.getCurrentDate(), DateTimeUtils.getCurrentDate());
	}
}
