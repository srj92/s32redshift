package co.md.bean;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import co.md.enums.ReportTypeEnum;

public class ReportRequest {

	private String startDate;
	private String endDate;
	private ReportTypeEnum reportType;

	public ReportRequest() {
		super();
	}
	
	public ReportTypeEnum getReportType() {
		return reportType;
	}

	public void setReportType(ReportTypeEnum reportType) {
		this.reportType = reportType;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public static ReportRequest fromJson(String json) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		ReportRequest request = mapper.readValue(json, ReportRequest.class);
		return request;
	}
	
	public static String toJson(ReportRequest reportRequest) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(reportRequest);
		return json;
	}

	@Override
	public String toString() {
		return "ReportRequest [startDate=" + startDate + ", endDate=" + endDate + ", reportType=" + reportType + "]";
	}
	
}