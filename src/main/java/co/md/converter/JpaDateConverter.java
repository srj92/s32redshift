package co.md.converter;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.AttributeConverter;

public class JpaDateConverter implements AttributeConverter<Date, Timestamp> {

	//private static final Logger LOG = LoggerFactory.getLogger(JpaDateConverter.class);
	
	@Override
	public Timestamp convertToDatabaseColumn(Date date) {
		if (date!=null) {
			Timestamp timestamp = new Timestamp(date.getTime());
			return timestamp;	
		}
		return null;
	}
	@Override
	public Date convertToEntityAttribute(Timestamp t) {
		if (t != null) {
			Date date = new Date(t.getTime());
			return date;	
		}
		return null;
	}

}
