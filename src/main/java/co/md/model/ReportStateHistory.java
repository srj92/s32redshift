package co.md.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import co.md.converter.JpaDateConverter;

@Entity
@Table(name = "report_state_history")
public class ReportStateHistory {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "cmp_id")
	private Long campaignId;
	
	@Column(name = "report_type")
	private String reportType;
	
	private Long state;
	
	@Column(name = "error_code")
	private Long errorCode;
	
	@Column(name = "error_msg", columnDefinition = "TEXT")
	private String errorMessage;
	
	@Column(name = "creation_date")
	@Convert(converter = JpaDateConverter.class)
	private Date creationDate;
	
	@Column(name = "modify_date")
	@Convert(converter = JpaDateConverter.class)
	private Date modifyDate;

	public Long getId() {
		return id;
	}

	public Long getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(Long campaignId) {
		this.campaignId = campaignId;
	}

	public String getReportType() {
		return reportType;
	}

	public void setReportType(String reportType) {
		this.reportType = reportType;
	}

	public Long getState() {
		return state;
	}

	public void setState(Long state) {
		this.state = state;
	}

	public Long getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(Long errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public ReportStateHistory() {
		super();
	}

	@Override
	public String toString() {
		return "ReportStateHistory [id=" + id + ", campaignId=" + campaignId + ", reportType=" + reportType + ", state="
				+ state + ", errorCode=" + errorCode + ", errorMessage=" + errorMessage + ", creationDate="
				+ creationDate + ", modifyDate=" + modifyDate + "]";
	}
	
}
