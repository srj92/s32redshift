package co.md.model;

public class DbConfig {
	
	private String driverClassName;
	private String dbUsername;
	private String dbPassword;
	private String dbUrl;
	
	public DbConfig(String driverClassName, String dbUsername, String dbPassword, String dbUrl) {
		super();
		this.driverClassName = driverClassName;
		this.dbUsername = dbUsername;
		this.dbPassword = dbPassword;
		this.dbUrl = dbUrl;
	}

	public DbConfig() {
		super();
	}

	public String getDriverClassName() {
		return driverClassName;
	}

	public void setDriverClassName(String driverClassName) {
		this.driverClassName = driverClassName;
	}

	public String getDbUsername() {
		return dbUsername;
	}

	public void setDbUsername(String dbUsername) {
		this.dbUsername = dbUsername;
	}

	public String getDbPassword() {
		return dbPassword;
	}

	public void setDbPassword(String dbPassword) {
		this.dbPassword = dbPassword;
	}

	public String getDbUrl() {
		return dbUrl;
	}

	public void setDbUrl(String dbUrl) {
		this.dbUrl = dbUrl;
	}

	@Override
	public String toString() {
		return "DbConfig [driverClassName=" + driverClassName + ", dbUsername=" + dbUsername + ", dbPassword="
				+ dbPassword + ", dbUrl=" + dbUrl + "]";
	}


}
