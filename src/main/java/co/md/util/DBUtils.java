package co.md.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import co.md.model.DbConfig;

public class DBUtils {
	
	public static Connection getConnection(DbConfig dbConfig) throws ClassNotFoundException, SQLException {
		String driverClassName = dbConfig.getDriverClassName();
		String url = dbConfig.getDbUrl();
		String username = dbConfig.getDbUsername();
		String password = dbConfig.getDbPassword();
		Class.forName(driverClassName);
		Connection connection = DriverManager.getConnection(url, username, password);
		return connection;
	}

}
