package co.md.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;

import co.md.bean.ReportRequest;
import co.md.enums.ErrorCodeEnum;
import co.md.exceptions.ClientException;

@Component
@Qualifier("reportGenerationSQSClient")
public class ReportGenerationSQSClient implements ReportGenerationClient {

	private static final Logger LOG = LoggerFactory.getLogger(ReportGenerationSQSClient.class);

	@Autowired
	protected JmsTemplate defaultJmsTemplate;

	@Value("${overview_queue}")
	private String overviewReportQueue;
	
	@Value("${os_queue}")
	private String osReportQueue;
	
	@Value("${publisher_queue}")
	private String publisherReportQueue;
	
	@Value("${creative_queue}")
	private String creativeReportQueue;
	
	@Value("${geo_queue}")
	private String geoReportQueue;
	
	@Value("${device_queue}")
	private String deviceReportQueue;
	
	@Value("${browser_queue}")
	private String browserReportQueue;
	
	@Value("${timeband_queue}")
	private String timebandReportQueue;
	
	@Value("${source_queue}")
	private String sourceReportQueue;
	
	@Value("${event_queue}")
	private String eventReportQueue;
	
	@Value("${error_queue}")
	private String errorReportQueue;

	public void sendMessage(ReportRequest reportRequest, String queue) {
		try {
			LOG.info("Sending to queue: {}", queue);
			defaultJmsTemplate.convertAndSend(queue, ReportRequest.toJson(reportRequest));
		} catch (JmsException | JsonProcessingException e) {
			ClientException ce = new ClientException(reportRequest, e);
			ce.setErrorCode(ErrorCodeEnum.APP_PROCESSING);
			throw ce;
		}
	}

	@Override
	public void generateOverviewReport(ReportRequest reportRequest) {
		sendMessage(reportRequest, overviewReportQueue);
	}

	@Override
	public void generatePublisherReport(ReportRequest reportRequest) {
		sendMessage(reportRequest, publisherReportQueue);
	}

	@Override
	public void generateCreativeReport(ReportRequest reportRequest) {
		sendMessage(reportRequest, creativeReportQueue);
	}

	@Override
	public void generateGeoReport(ReportRequest reportRequest) {
		sendMessage(reportRequest, geoReportQueue);
	}

	@Override
	public void generateDeviceReport(ReportRequest reportRequest) {
		sendMessage(reportRequest, deviceReportQueue);
	}

	@Override
	public void generateOSReport(ReportRequest reportRequest) {
		sendMessage(reportRequest, osReportQueue);
	}

	@Override
	public void generateTimebandReport(ReportRequest reportRequest) {
		sendMessage(reportRequest, timebandReportQueue);
	}

	@Override
	public void generateBrowserReport(ReportRequest reportRequest) {
		sendMessage(reportRequest, browserReportQueue);
	}

	@Override
	public void generateSourceReport(ReportRequest reportRequest) {
		sendMessage(reportRequest, sourceReportQueue);
	}
	
	@Override
	public void generateEventReport(ReportRequest reportRequest) {
		sendMessage(reportRequest, eventReportQueue);
	}
	
	@Override
	public void generateErrorReport(ReportRequest reportRequest) {
		sendMessage(reportRequest, errorReportQueue);
	}

}
