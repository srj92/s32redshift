package co.md.client;

import co.md.bean.ReportRequest;

public interface ReportGenerationClient {

	public abstract void generateOverviewReport(ReportRequest reportRequest);
	public abstract void generatePublisherReport(ReportRequest reportRequest);
	public abstract void generateCreativeReport(ReportRequest reportRequest);
	public abstract void generateGeoReport(ReportRequest reportRequest);
	public abstract void generateDeviceReport(ReportRequest reportRequest);
	public abstract void generateOSReport(ReportRequest reportRequest);
	public abstract void generateTimebandReport(ReportRequest reportRequest);
	public abstract void generateBrowserReport(ReportRequest reportRequest);
	public abstract void generateSourceReport(ReportRequest reportRequest);
	public abstract void generateEventReport(ReportRequest reportRequest);
	public abstract void generateErrorReport(ReportRequest reportRequest);

}