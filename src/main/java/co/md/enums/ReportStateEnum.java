package co.md.enums;

public enum ReportStateEnum {

	SCHEDULER_FAIL(10l),
	SCHEDULER_SUCCESS(11l);

	private Long reportState;
	
	private ReportStateEnum(Long reportState) {
		this.reportState = reportState;
	}

	public Long getReportState() {
		return reportState;
	}

	public void setReportState(Long reportState) {
		this.reportState = reportState;
	}

}
