package co.md.enums;

public enum ErrorCodeEnum {
	
	DB_CONNECTION_FAIL(1000L),
	APP_PROCESSING(4000L);

	private Long errorCode;
	
	private ErrorCodeEnum(Long errorCode) {
		this.errorCode = errorCode;
	}

	public Long getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(Long errorCode) {
		this.errorCode = errorCode;
	}

	
}
