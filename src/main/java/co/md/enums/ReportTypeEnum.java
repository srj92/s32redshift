package co.md.enums;

public enum ReportTypeEnum {
	
	OVERVIEW(1,"overview"),
    PUBLISHER(2,"publisher"),
    CREATIVE(3,"creative"),
    GEO(4,"geo"),
    DEVICE(5,"device"),
    OS(6,"os"),
    BROWSER(7,"browser"),
    TIMEBAND(8,"timeband"),
    SOURCE(9,"source"),
    EVENT(10,"event"),
    ERROR(11,"error");

	private Integer id;
	private String reportName;
	
	private ReportTypeEnum(Integer id, String reportName) {
		this.id = id;
		this.reportName = reportName;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String name) {
		this.reportName = name;
	}

}
