package co.md.common;

public class Constants {

	// DB props
	public static final String USER = "user";
	public static final String PASSWORD = "password";

	public static final String TABLE_NAME_PREFIX = "engine";
	public static final String TABLE_TEMPLATE_NAME = "engine";
	public static final String S3_PREFIX_NAME = "engine/";
	public static final String STREAM_PREFIX = "md-engine-log";

}