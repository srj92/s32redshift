package co.md.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import co.md.model.DbConfig;

@Configuration
public class AppConfig {

	@Value("${access-key}")
	private String accessKey;

	@Value("${secret-key}")
	private String secretKey;
	
	@Value("${s3-location}")
	private String s3Location;
	
	@Value("${driver-class}")
	private String driverClass;
	
	@Value("${db-username-value}")
	private String dbUsername;
	
	@Value("${db-password-value}")
	private String dbPassword;
	
	@Value("${db-url}")
	private String dbUrl;
	
	@Value("${firehoseRegion}")
	private String firehoseRegion;
	
	@Bean
	public String firehoseRegion() {
		return firehoseRegion;
	}
	
	@Bean
	public String accessKey() {
		return accessKey;
	}
	
	@Bean
	public String secretKey() {
		return secretKey;
	}
	
	@Bean
	public String s3Location() {
		return s3Location;
	}
	
	@Bean
	public String driverClassName() {
		return driverClass;
	}
	
	@Bean
	public String dbUsername() {
		return dbUsername;
	}
	
	@Bean
	public String dbPassword() {
		return dbPassword;
	}
	
	@Bean
	public String dbUrl() {
		return dbUrl;
	}
	
	@Bean
	public DbConfig dbConfig() {
		DbConfig dbConfig = new DbConfig();
		dbConfig.setDriverClassName(driverClassName());
		dbConfig.setDbUrl(dbUrl());
		dbConfig.setDbUsername(dbUsername());
		dbConfig.setDbPassword(dbPassword());
		return dbConfig;	
	}
	
}