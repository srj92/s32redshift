package co.md.exceptions;

import co.md.bean.ReportRequest;
import co.md.enums.ErrorCodeEnum;

public class ClientException extends AnalyticsMasterException {

	private static final long serialVersionUID = 1L;
	
	private ReportRequest payload;

	public ClientException(ReportRequest payload ,Throwable cause) {
		super(cause);
		this.setPayload(payload);
		super.setMessage(cause.getMessage());
	}
	
	public ReportRequest getPayload() {
		return payload;
	}

	public void setPayload(ReportRequest payload) {
		this.payload = payload;
	}

	public String getMessage() {
		return super.getMessage();
	}

	public void setMessage(String message) {
		super.setMessage(message);
	}
	
	public ErrorCodeEnum getErrorCode() {
		return super.getErrorCode();
	}

	public void setErrorCode(ErrorCodeEnum errorCode) {
		super.setErrorCode(errorCode);
	}

	@Override
	public String toString() {
		return "ClientException [payload=" + payload + "]";
	}
	
}
