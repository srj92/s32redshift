package co.md.exceptions;

import co.md.enums.ErrorCodeEnum;

public class AnalyticsMasterException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	private ErrorCodeEnum errorCode;
	private String message;
	
	public AnalyticsMasterException(Throwable cause) {
		super(cause);
	}
	
	public AnalyticsMasterException(String message) {
		super();
		this.message = message;
	}
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ErrorCodeEnum getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(ErrorCodeEnum errorCode) {
		this.errorCode = errorCode;
	}

	@Override
	public String toString() {
		return "S32RedshiftException [errorCode=" + errorCode + ", message=" + message + "]";
	}
	
	
}
