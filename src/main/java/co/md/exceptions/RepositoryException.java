package co.md.exceptions;

import co.md.enums.ErrorCodeEnum;

public class RepositoryException extends AnalyticsMasterException {

	private static final long serialVersionUID = 1L;
	
	public RepositoryException(Throwable cause) {
		super(cause);
		super.setMessage(cause.getMessage());
	}
	
	public String getMessage() {
		return super.getMessage();
	}

	public void setMessage(String message) {
		super.setMessage(message);
	}
	
	public ErrorCodeEnum getErrorCode() {
		return super.getErrorCode();
	}

	public void setErrorCode(ErrorCodeEnum errorCode) {
		super.setErrorCode(errorCode);
	}

	@Override
	public String toString() {
		return "RepositoryException [getMessage()=" + getMessage() + ", getErrorCode()=" + getErrorCode() + "]";
	}
	
}
