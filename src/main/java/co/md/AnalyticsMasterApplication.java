package co.md;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 
 * @author suraj
 *
 */
@SpringBootApplication
@EnableScheduling
public class AnalyticsMasterApplication {

	public static void main(String[] args) {
		SpringApplication.run(AnalyticsMasterApplication.class, args);
	}
}
