package co.md.repository;

public interface RedshiftRepository {

	boolean copyFromS3ToRedshift(String username, String password, String dbUrl);
	
	boolean createTable(String tableName, String tableTemplate);
	
	boolean dropTable(String tableName);
}
