package co.md.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import co.md.model.ReportStateHistory;


@Repository
public interface ReportStateHistoryRepository extends CrudRepository<ReportStateHistory, Integer>{}
