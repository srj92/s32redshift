package co.md.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.md.enums.ErrorCodeEnum;
import co.md.exceptions.RepositoryException;
import co.md.model.DbConfig;
import co.md.util.DBUtils;

@Repository
public class RedshiftRepositoryImpl implements RedshiftRepository {

	private static final Logger LOG = LoggerFactory.getLogger(RedshiftRepositoryImpl.class);

	@Autowired
	private DbConfig dbConfig;

	public boolean copyFromS3ToRedshift(String username, String password, String dbUrl) {
		return false;
		/*boolean result = false;
		LOG.info("Copying data from S3 to Redshift");

		Properties props = new Properties();
		props.setProperty(USER, username);
		props.setProperty(PASSWORD, password);

		try (Connection connection = DriverManager.getConnection(dbUrl, props);
				Statement statement = connection.createStatement()) {

			Class.forName(driverClass);
			String sql = "COPY engine FROM " + s3Location + " CREDENTIALS 'aws_access_key_id=" + accessKey
					+ ";aws_secret_access_key=" + secretKey + "'  GZIP DELIMITER '|' REMOVEQUOTES;";
			LOG.info("Executing query -----> {}", sql);

			result = statement.execute(sql);

		} catch (SQLException | ClassNotFoundException e) {
			LOG.error("Exception occured: ", e);
			throwRepositoryException(e);
		}
		LOG.info("Finished copy process");

		return result;
*/	}

	@Override
	public boolean createTable(String tableName, String tableTemplate) {

		boolean result = false;
		try {
			Connection redshiftConnection = DBUtils.getConnection(dbConfig);

			PreparedStatement clkImpLogsPreparedStatement = redshiftConnection
					.prepareStatement("create table if not exists " + tableName + "(like " + tableTemplate + ");");

			clkImpLogsPreparedStatement.execute();
			result = true;
		} catch (ClassNotFoundException | SQLException e) {
			LOG.error("Exception occured: ", e);
			throwRepositoryException(e);
		}
		return result;
	}
	
	@Override
	public boolean dropTable(String tableName) {

		boolean result = false;
		try {
			Connection redshiftConnection = DBUtils.getConnection(dbConfig);

			PreparedStatement clkImpLogsPreparedStatement = redshiftConnection
					.prepareStatement("drop table " + tableName + ";");

			clkImpLogsPreparedStatement.execute();
			result = true;
			
		} catch (ClassNotFoundException | SQLException e) {
			LOG.error("Exception occured: ", e);
			throwRepositoryException(e);
		}
		return result;
	}

	private void throwRepositoryException(Exception e) {
		RepositoryException re = new RepositoryException(e);
		re.setErrorCode(ErrorCodeEnum.DB_CONNECTION_FAIL);
		throw re;
	}

}