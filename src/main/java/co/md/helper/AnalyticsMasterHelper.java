package co.md.helper;

import static co.md.enums.ErrorCodeEnum.APP_PROCESSING;
import static co.md.enums.ReportStateEnum.SCHEDULER_FAIL;
import static co.md.enums.ReportStateEnum.SCHEDULER_SUCCESS;

import java.util.Date;

import org.springframework.stereotype.Component;

import co.md.bean.ReportRequest;
import co.md.exceptions.ClientException;
import co.md.exceptions.RepositoryException;
import co.md.model.ReportStateHistory;
import co.md.util.DateTimeUtils;

@Component
public class AnalyticsMasterHelper {

	public ReportStateHistory generateReportStateHistory() {
		Date currentDate = DateTimeUtils.getCurrentDate();
		
		ReportStateHistory reportStateHistory = new ReportStateHistory();
		reportStateHistory.setCreationDate(currentDate);
		reportStateHistory.setModifyDate(currentDate);
		reportStateHistory.setState(SCHEDULER_SUCCESS.getReportState());
		return reportStateHistory;
	}
	
	public ReportStateHistory generateErrorReportStateHistory(Exception e) {
		Date currentDate = DateTimeUtils.getCurrentDate();
		
		ReportStateHistory reportStateHistory = new ReportStateHistory();
		reportStateHistory.setCreationDate(currentDate);
		reportStateHistory.setModifyDate(currentDate);
		reportStateHistory.setState(SCHEDULER_FAIL.getReportState());
		if (e != null) {
			if (e instanceof RepositoryException) {
				RepositoryException re = (RepositoryException) e;
				reportStateHistory.setErrorCode(re.getErrorCode().getErrorCode());
				reportStateHistory.setErrorMessage(re.getCause().getMessage());
			}
			else if (e instanceof ClientException) {
				ClientException ce = (ClientException) e;
				reportStateHistory.setErrorCode(ce.getErrorCode().getErrorCode());
				reportStateHistory.setErrorMessage(ce.getCause().getMessage());
				reportStateHistory.setReportType(ce.getPayload().getReportType().getReportName());
			} 
			else {
				reportStateHistory.setErrorCode(APP_PROCESSING.getErrorCode());
				reportStateHistory.setErrorMessage(e.getMessage());
			}
		}
		return reportStateHistory;
	}

	public ReportRequest generateReportRequest(Date startDate, Date endDate) {
		ReportRequest reportRequest = new ReportRequest();
		reportRequest.setStartDate(DateTimeUtils.getDateAsString(startDate));
		reportRequest.setEndDate(DateTimeUtils.getDateAsString(endDate));
		return reportRequest;
	}
}
