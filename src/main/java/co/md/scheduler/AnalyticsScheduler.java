package co.md.scheduler;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import co.md.service.AbstractAnalyticsService;
import co.md.util.DateTimeUtils;

@Component
public class AnalyticsScheduler {

	private static final Logger LOG = LoggerFactory.getLogger(AnalyticsScheduler.class);

	@Autowired
	private AbstractAnalyticsService s32RedshiftService;

/*	@Autowired
	private RedshiftService redshiftService;

	@Autowired
	private FirehoseService firehoseService;
*/
	// @Scheduled(cron = "${cron-expression}")
	public void triggerAnalyticsServiceJob() {
		Date currentDate = DateTimeUtils.getCurrentDate();
		LOG.info("Triggering analytics service: ", currentDate);
		s32RedshiftService.process(currentDate, currentDate);
	}

/*	public void triggerFirehoseAndRedshiftJob() throws Exception {
		LOG.info("Creating Redshift table for next day");
		redshiftService.createTableForNextDay();

		String deliveryStreamName = STREAM_PREFIX + getNextDate(getDateAsString(getCurrentDate()));
		LOG.info("Creating Firehose stream: " + deliveryStreamName);
		firehoseService.createDeliveryStream(deliveryStreamName);

		deliveryStreamName = STREAM_PREFIX + getPreviousDate(getDateAsString(getCurrentDate()));
		LOG.info("Deleting Firehose stream: " + deliveryStreamName);
		firehoseService.deleteDeliveryStream(deliveryStreamName);

	}*/

}