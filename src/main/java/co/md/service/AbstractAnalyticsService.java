package co.md.service;

import java.util.Date;

public abstract class AbstractAnalyticsService {
	
	public void process(Date startDate, Date endDate) {
		Boolean isCopied = copyFromS3ToRedshift();
		if (isCopied) {
			Boolean allTriggered = generateAllReports(startDate, endDate);
			updateApplicationStateHistory(allTriggered);
		}
	}
	
	protected abstract Boolean copyFromS3ToRedshift();
	protected abstract Boolean generateAllReports(Date startDate, Date endDate);
	protected abstract void updateApplicationStateHistory(Boolean status);
}
