package co.md.service;

import static co.md.common.Constants.TABLE_NAME_PREFIX;
import static co.md.common.Constants.TABLE_TEMPLATE_NAME;
import static co.md.util.DateTimeUtils.getCurrentDate;
import static co.md.util.DateTimeUtils.getDateAsString;
import static co.md.util.DateTimeUtils.getNextDate;

import java.text.ParseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.md.repository.RedshiftRepository;

@Service
public class RedshiftServiceImpl implements RedshiftService {

	private static final Logger LOG = LoggerFactory.getLogger(RedshiftServiceImpl.class);

	@Autowired
	private RedshiftRepository redshiftRepository;

	@Override
	public void createTableForNextDay() {
		LOG.info("Creating a new table for next day");

		String tableName = null;
		try {
			tableName = TABLE_NAME_PREFIX + getNextDate(getDateAsString(getCurrentDate()));
			redshiftRepository.createTable(tableName, TABLE_TEMPLATE_NAME);
		} catch (ParseException e) {
			LOG.error("Exception occured: ", e);
			return;
		}
	}

	@Override
	public void dropTable(String tableName) {
		LOG.info("Dropping table: " + tableName);
		redshiftRepository.dropTable(tableName);
	}

}
