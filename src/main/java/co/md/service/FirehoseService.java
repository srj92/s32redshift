/*package co.md.service;

import static co.md.common.Constants.S3_PREFIX_NAME;
import static co.md.util.DateTimeUtils.getCurrentDate;
import static co.md.util.DateTimeUtils.getDateAsString;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.RegionUtils;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagement;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagementClient;
import com.amazonaws.services.identitymanagement.model.GetRoleRequest;
import com.amazonaws.services.kinesisfirehose.AmazonKinesisFirehoseClient;
import com.amazonaws.services.kinesisfirehose.model.BufferingHints;
import com.amazonaws.services.kinesisfirehose.model.CopyCommand;
import com.amazonaws.services.kinesisfirehose.model.CreateDeliveryStreamRequest;
import com.amazonaws.services.kinesisfirehose.model.DeleteDeliveryStreamRequest;
import com.amazonaws.services.kinesisfirehose.model.DeliveryStreamDescription;
import com.amazonaws.services.kinesisfirehose.model.DescribeDeliveryStreamRequest;
import com.amazonaws.services.kinesisfirehose.model.DescribeDeliveryStreamResult;
import com.amazonaws.services.kinesisfirehose.model.ListDeliveryStreamsRequest;
import com.amazonaws.services.kinesisfirehose.model.ListDeliveryStreamsResult;
import com.amazonaws.services.kinesisfirehose.model.RedshiftDestinationConfiguration;
import com.amazonaws.services.kinesisfirehose.model.S3DestinationConfiguration;
import com.amazonaws.util.StringUtils;

import co.md.util.DateTimeUtils;

@Service
public class FirehoseService {

	private final Logger LOG = LoggerFactory.getLogger(FirehoseService.class);
	private final String CONFIG_FILE = "firehosetoredshift.properties";
	private final String S3_ARN_PREFIX = "arn:aws:s3:::";

	@Autowired
	private AmazonKinesisFirehoseClient firehoseClient;

	@Autowired
	private String firehoseRegion;
	@Autowired
	private String accessKey;
	@Autowired
	private String secretKey;

	private Properties properties;
	private String s3BucketName;
	private String s3BucketARN;
	private Integer s3DestinationSizeInMBs;
	private Integer s3DestinationIntervalInSeconds;
	private String clusterJDBCUrl;
	private String username;
	private String password;
	private String dataTableName;
	private String iamRoleName;
	private AmazonIdentityManagement iamClient;

	*//**
	 * Load the input parameters from properties file.
	 *
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ParseException 
	 *//*
	@SuppressWarnings("deprecation")
	@PostConstruct
	private void loadConfig() throws FileNotFoundException, IOException, ParseException {
		try (InputStream configStream = Thread.currentThread().getContextClassLoader()
				.getResourceAsStream(CONFIG_FILE)) {
			if (configStream == null) {
				throw new FileNotFoundException();
			}
			properties = new Properties();
			properties.load(configStream);
		}

		// Read properties
		s3BucketName = properties.getProperty("s3BucketName").trim();
		s3BucketARN = getBucketARN(s3BucketName);

		String sizeInMBsProperty = properties.getProperty("destinationSizeInMBs");
		s3DestinationSizeInMBs = StringUtils.isNullOrEmpty(sizeInMBsProperty) ? null
				: Integer.parseInt(sizeInMBsProperty.trim());

		String intervalInSecondsProperty = properties.getProperty("destinationIntervalInSeconds");
		s3DestinationIntervalInSeconds = StringUtils.isNullOrEmpty(intervalInSecondsProperty) ? null
				: Integer.parseInt(intervalInSecondsProperty.trim());

		clusterJDBCUrl = properties.getProperty("clusterJDBCUrl");
		username = properties.getProperty("username");
		password = properties.getProperty("password");
		dataTableName = properties.getProperty("dataTableName");
		
		iamRoleName = properties.getProperty("iamRoleName");

		AWSCredentials credentials = null;
		try {
			credentials = new BasicAWSCredentials(accessKey, secretKey);
		} catch (Exception e) {
			throw new AmazonClientException("Cannot load the credentials from the credential properties. ", e);
		}

		iamClient = new AmazonIdentityManagementClient(credentials);
		iamClient.setRegion(RegionUtils.getRegion(firehoseRegion));

	}

	*//**
	 * Method to create delivery stream with Redshift destination configuration.
	 *
	 * @throws Exception
	 *//*
	public void createDeliveryStream(String deliveryStreamName) throws Exception {
		boolean deliveryStreamExists = false;
		LOG.info("Checking if " + deliveryStreamName + " already exits");
		List<String> deliveryStreamNames = listDeliveryStreams();
		if (deliveryStreamNames != null && deliveryStreamNames.contains(deliveryStreamName)) {
			deliveryStreamExists = true;
			LOG.info("DeliveryStream " + deliveryStreamName + " already exists. Not creating the new delivery stream");
		} else {
			LOG.info("DeliveryStream " + deliveryStreamName + " does not exist");
		}

		if (!deliveryStreamExists) {
			// Create DeliveryStream
			CreateDeliveryStreamRequest createDeliveryStreamRequest = new CreateDeliveryStreamRequest();
			createDeliveryStreamRequest.setDeliveryStreamName(deliveryStreamName);

			S3DestinationConfiguration redshiftS3Configuration = new S3DestinationConfiguration();
			redshiftS3Configuration.setBucketARN(s3BucketARN);
			redshiftS3Configuration.setPrefix(S3_PREFIX_NAME);
			redshiftS3Configuration.setCompressionFormat("GZIP");

			BufferingHints bufferingHints = null;
			if (s3DestinationSizeInMBs != null || s3DestinationIntervalInSeconds != null) {
				bufferingHints = new BufferingHints();
				bufferingHints.setSizeInMBs(s3DestinationSizeInMBs);
				bufferingHints.setIntervalInSeconds(s3DestinationIntervalInSeconds);
			}
			
			redshiftS3Configuration.setBufferingHints(bufferingHints);

			String iamRoleArn = getIAMRoleArn();
			redshiftS3Configuration.setRoleARN(iamRoleArn);

			CopyCommand copyCommand = new CopyCommand();
			copyCommand.withDataTableName(dataTableName + DateTimeUtils.getNextDate(getDateAsString(getCurrentDate())));

			RedshiftDestinationConfiguration redshiftDestinationConfiguration = new RedshiftDestinationConfiguration();
			redshiftDestinationConfiguration.withClusterJDBCURL(clusterJDBCUrl).withRoleARN(iamRoleArn)
					.withUsername(username).withPassword(password).withCopyCommand(copyCommand)
					.withS3Configuration(redshiftS3Configuration);

			createDeliveryStreamRequest.setRedshiftDestinationConfiguration(redshiftDestinationConfiguration);

			firehoseClient.createDeliveryStream(createDeliveryStreamRequest);
			Thread.sleep(5000);

			// The Delivery Stream is now being created.
			LOG.info("Creating DeliveryStream : " + deliveryStreamName);
			waitForDeliveryStreamToBecomeAvailable(deliveryStreamName);
		}
	}

	*//**
	 * Method to list all the delivery streams in the customer account.
	 *
	 * @return the collection of delivery streams
	 *//*
	public List<String> listDeliveryStreams() {
		ListDeliveryStreamsRequest listDeliveryStreamsRequest = new ListDeliveryStreamsRequest();
		ListDeliveryStreamsResult listDeliveryStreamsResult = firehoseClient
				.listDeliveryStreams(listDeliveryStreamsRequest);

		List<String> deliveryStreamNames = listDeliveryStreamsResult.getDeliveryStreamNames();

		while (listDeliveryStreamsResult.isHasMoreDeliveryStreams()) {
			if (deliveryStreamNames.size() > 0) {
				listDeliveryStreamsRequest
						.setExclusiveStartDeliveryStreamName(deliveryStreamNames.get(deliveryStreamNames.size() - 1));
			}

			listDeliveryStreamsResult = firehoseClient.listDeliveryStreams(listDeliveryStreamsRequest);
			deliveryStreamNames.addAll(listDeliveryStreamsResult.getDeliveryStreamNames());
		}
		return deliveryStreamNames;
	}

	private String getBucketARN(String bucketName) throws IllegalArgumentException {
		return new StringBuilder().append(S3_ARN_PREFIX).append(bucketName).toString();
	}

	*//**
	 * Method to get IAM role arn.
	 *
	 * @return the role ARN
	 * @throws InterruptedException
	 *//*
	private String getIAMRoleArn() throws InterruptedException {
		String roleARN = iamClient.getRole(new GetRoleRequest().withRoleName(iamRoleName)).getRole().getArn();
		return roleARN;
	}

	*//**
	 * Method to wait until the delivery stream becomes active.
	 *
	 * @param deliveryStreamName
	 *            the delivery stream
	 * @throws Exception
	 *//*
	private void waitForDeliveryStreamToBecomeAvailable(String deliveryStreamName) throws Exception {

		LOG.info("Waiting for " + deliveryStreamName + " to become ACTIVE...");

		long startTime = System.currentTimeMillis();
		long endTime = startTime + (10 * 60 * 1000);
		while (System.currentTimeMillis() < endTime) {
			try {
				Thread.sleep(1000 * 20);
			} catch (InterruptedException e) {
				// Ignore interruption (doesn't impact deliveryStream creation)
			}

			DeliveryStreamDescription deliveryStreamDescription = describeDeliveryStream(deliveryStreamName);
			String deliveryStreamStatus = deliveryStreamDescription.getDeliveryStreamStatus();
			LOG.info("  - current state: " + deliveryStreamStatus);
			if (deliveryStreamStatus.equals("ACTIVE")) {
				return;
			}
		}
		throw new AmazonServiceException("DeliveryStream " + deliveryStreamName + " never went active");
	}

	*//**
	 * Method to describe the delivery stream.
	 *
	 * @param deliveryStreamName
	 *            the delivery stream
	 * @return the delivery description
	 *//*
	private DeliveryStreamDescription describeDeliveryStream(String deliveryStreamName) {
		DescribeDeliveryStreamRequest describeDeliveryStreamRequest = new DescribeDeliveryStreamRequest();
		describeDeliveryStreamRequest.withDeliveryStreamName(deliveryStreamName);
		DescribeDeliveryStreamResult describeDeliveryStreamResponse = firehoseClient
				.describeDeliveryStream(describeDeliveryStreamRequest);
		return describeDeliveryStreamResponse.getDeliveryStreamDescription();
	}


	*//**
	 * Method to delete the delivery stream.
	 *
	 * @param deliveryStreamName
	 *            the delivery stream
	 * @return the delivery description
	 *//*
	
	public void deleteDeliveryStream(String deliveryStreamName) {
		LOG.info("Deleting stream: " + deliveryStreamName);
		DeleteDeliveryStreamRequest deleteHoseRequest= new DeleteDeliveryStreamRequest(); 
        deleteHoseRequest.setDeliveryStreamName(deliveryStreamName); 
        firehoseClient.deleteDeliveryStream(deleteHoseRequest); 
	}

}
*/