package co.md.service;

public interface RedshiftService {

	void createTableForNextDay();
	void dropTable(String tableName); 
}
