package co.md.service;

import co.md.model.ReportStateHistory;

public interface ReportStateHistoryService {
	
	void updateStateHistory(ReportStateHistory reportStateHistory);
}
