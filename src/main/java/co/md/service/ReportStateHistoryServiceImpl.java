package co.md.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.md.model.ReportStateHistory;
import co.md.repository.ReportStateHistoryRepository;


@Service
public class ReportStateHistoryServiceImpl implements ReportStateHistoryService {

	private static final Logger LOG = LoggerFactory.getLogger(ReportStateHistoryServiceImpl.class);

	@Autowired
	private ReportStateHistoryRepository reportStateHistoryRepository;

	@Override
	public void updateStateHistory(ReportStateHistory reportStateHistory) {
		LOG.info("Updating application status");
		reportStateHistoryRepository.save(reportStateHistory);
	}

}
