package co.md.service;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import co.md.bean.ReportRequest;
import co.md.client.ReportGenerationClient;
import co.md.enums.ReportTypeEnum;
import co.md.exceptions.ClientException;
import co.md.helper.AnalyticsMasterHelper;
import co.md.model.ReportStateHistory;

@Service
public class AnalyticsService extends AbstractAnalyticsService {

	private static final Logger LOG = LoggerFactory.getLogger(AnalyticsService.class);

	@Autowired
	private AnalyticsMasterHelper helper;

	@Autowired
	private ReportStateHistoryService reportStateHistoryService;

	@Autowired
	@Qualifier("reportGenerationSQSClient")
	private ReportGenerationClient reportGenerationSQSClient;

	@Override
	public Boolean copyFromS3ToRedshift() {
		try {
			/*
			 * LOG.info("Moving data from S3 to Redshift");
			 * 
			 * String dbUrl =
			 * "jdbc:redshift://mdlogs.cvkxxjip0qbg.us-east-1.redshift.amazonaws.com:5439/engine";
			 * String username = "md"; String password = "Mangodata123$";
			 * 
			 * redshiftRepository.copyFromS3ToRedshift(username, password,
			 * dbUrl);
			 */
		} catch (Exception e) {
			LOG.error("Exception occured: ", e);
			LOG.error("Updating error state");
			ReportStateHistory reportStateHistory = helper.generateErrorReportStateHistory(e);
			reportStateHistoryService.updateStateHistory(reportStateHistory);
		}
		return true;
	}

	@Override
	protected Boolean generateAllReports(Date startDate, Date endDate) {
		Boolean allTriggered = true;
		ReportRequest reportRequest = helper.generateReportRequest(startDate, endDate);
		LOG.info("Triggering all reports");
		try {
			reportRequest.setReportType(ReportTypeEnum.BROWSER);
			reportGenerationSQSClient.generateBrowserReport(reportRequest);
		} catch (ClientException e) {
			handleException(e, allTriggered);
		}
		try {
			reportRequest.setReportType(ReportTypeEnum.CREATIVE);
			reportGenerationSQSClient.generateCreativeReport(reportRequest);
		} catch (ClientException e) {
			handleException(e, allTriggered);
		}
		try {
			reportRequest.setReportType(ReportTypeEnum.DEVICE);
			reportGenerationSQSClient.generateDeviceReport(reportRequest);
		} catch (ClientException e) {
			handleException(e, allTriggered);
		}
		try {
			reportRequest.setReportType(ReportTypeEnum.GEO);
			reportGenerationSQSClient.generateGeoReport(reportRequest);
		} catch (ClientException e) {
			handleException(e, allTriggered);
		}
		try {
			reportRequest.setReportType(ReportTypeEnum.OS);
			reportGenerationSQSClient.generateOSReport(reportRequest);
		} catch (ClientException e) {
			handleException(e, allTriggered);
		}
		try {
			reportRequest.setReportType(ReportTypeEnum.OVERVIEW);
			reportGenerationSQSClient.generateOverviewReport(reportRequest);
		} catch (ClientException e) {
			handleException(e, allTriggered);
		}
		try {
			reportRequest.setReportType(ReportTypeEnum.PUBLISHER);
			reportGenerationSQSClient.generatePublisherReport(reportRequest);
		} catch (ClientException e) {
			handleException(e, allTriggered);
		}
		try {
			reportRequest.setReportType(ReportTypeEnum.SOURCE);
			reportGenerationSQSClient.generateSourceReport(reportRequest);
		} catch (ClientException e) {
			handleException(e, allTriggered);
		}
		try {
			reportRequest.setReportType(ReportTypeEnum.TIMEBAND);
			reportGenerationSQSClient.generateTimebandReport(reportRequest);
		} catch (ClientException e) {
			handleException(e, allTriggered);
		}
		try {
			reportRequest.setReportType(ReportTypeEnum.EVENT);
			reportGenerationSQSClient.generateEventReport(reportRequest);
		} catch (ClientException e) {
			handleException(e, allTriggered);
		}
		try {
			reportRequest.setReportType(ReportTypeEnum.ERROR);
			reportGenerationSQSClient.generateErrorReport(reportRequest);
		} catch (ClientException e) {
			handleException(e, allTriggered);
		}
		return allTriggered;
	}

	private void handleException(ClientException e, Boolean allTriggered) {
		allTriggered = false;
		LOG.error("Exception occured: ", e);
		ReportStateHistory reportStateHistory = helper.generateErrorReportStateHistory(e);
		reportStateHistoryService.updateStateHistory(reportStateHistory);
	}

	@Override
	protected void updateApplicationStateHistory(Boolean allTriggered) {
		ReportStateHistory reportStateHistory = null;
		if (allTriggered) {
			reportStateHistory = helper.generateReportStateHistory();
		} else {
			reportStateHistory = helper.generateErrorReportStateHistory(null);
		}
		reportStateHistoryService.updateStateHistory(reportStateHistory);
	}

}